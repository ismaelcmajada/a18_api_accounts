let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let accountSchema = new Schema ({

    name: String,
    balance: Number
  
});


accountSchema.statics.allAccounts = function (cb) {
    return this.find({}, cb);
}
      
accountSchema.statics.add = function (account, cb) {
    return this.create(account, cb);
}
      
accountSchema.statics.findById = function (account_id, cb) {
    return this.findOne({_id: account_id}, cb);
}
      
accountSchema.statics.removeById = function (account_id, cb) {
    return this.deleteOne({_id: account_id}, cb);
}

module.exports = mongoose.model("Account", accountSchema);