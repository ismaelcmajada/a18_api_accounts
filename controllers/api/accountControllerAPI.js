let Account = require("../../models/Account");

exports.account_list = function(req, res) {

    Account.allAccounts(function (err, accounts) {
        if (err) res.send(500, err.message);

        res.status(200).json ({
            accounts: accounts
    
        });
       
      })
    
}

exports.account_create = function(req, res) {

    let account = new Account({
        name: req.body.name,
        balance: req.body.balance,
  
    });
    
    Account.add(account, function (err,newAccount) {
    
    if (err) res.send(500, err.message);
    
        res.status(201).json({
            account: newAccount
        });
      });
  }

exports.account_delete = function(req, res) {

    Account.removeById(req.params._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.account_update = function(req, res) {

    Account.findById(req.params._id, function (err, account) {
        if (err) res.send(500, err.message);
    
        account.name = req.body.name;
        account.balance = req.body.balance;
    
        account.save();
    
        res.status(200).json({
            account: account
        });
    });
}