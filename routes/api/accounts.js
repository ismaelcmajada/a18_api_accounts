let express = require('express');

let router = express.Router();

let accountControllerAPI= require("../../controllers/api/accountControllerAPI");

router.get("/", accountControllerAPI.account_list);
router.post("/", accountControllerAPI.account_create);
router.delete("/:_id", accountControllerAPI.account_delete);
router.put("/:_id", accountControllerAPI.account_update);

module.exports = router;